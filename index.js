const express = require('express')
const app = express()
const port = 8080

let play = "v";

app.get('/', (req, res) => {
  res.send(play);
})

app.get('/delsi', (req, res) => {
  play = "d";
  res.send();
})

app.get('/kratsi', (req, res) => {
  play = "k";
  res.send();
})

app.get('/vypnout', (req, res) => {
  play = "v";
  res.send();
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})